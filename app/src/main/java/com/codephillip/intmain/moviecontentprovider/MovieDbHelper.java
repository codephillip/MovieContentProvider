package com.codephillip.intmain.moviecontentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by codephillip on 10/26/15.
 */
public class MovieDbHelper extends SQLiteOpenHelper {
    public static final int VERSION = 1;
    public static final String DATABASE_NAME = "movieDatabase";

    private static final String SQL_CREATE_DRAMA_TABLE = "CREATE TABLE "+ MovieContract.Drama.TABLE_NAME +" ( "
            + MovieContract.Drama._ID + " INTEGER PRIMARY KEY, "+ MovieContract.Drama.TITLE +" TINYTEXT, "
            + MovieContract.Drama.ACTOR+" TINYTEXT, "
            + MovieContract.Drama.DURATION +" TINYTEXT " + " );";
    private static final String SQL_CREATE_SCIFI_TABLE = "CREATE TABLE "+ MovieContract.SciFi.TABLE_NAME +" ( "
            + MovieContract.SciFi._ID + " INTEGER PRIMARY KEY, "+ MovieContract.SciFi.TITLE +" TINYTEXT, "
            + MovieContract.SciFi.ACTOR+" TINYTEXT, "
            + MovieContract.SciFi.DURATION +" TINYTEXT " + " );";



    public MovieDbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_DRAMA_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_SCIFI_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
