package com.codephillip.intmain.moviecontentprovider;

import android.content.ContentValues;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int n;
        for (n = 0 ; n < 6; n++){
            ContentValues contentValues = new ContentValues();
            contentValues.put(MovieContract.Drama.TITLE, "android inception "+String.valueOf(n));
            contentValues.put(MovieContract.Drama.DURATION, "180 "+String.valueOf(n));
            contentValues.put(MovieContract.Drama.ACTOR, "codephillip "+String.valueOf(n));


            Uri uri = getContentResolver().insert(MovieContract.SciFi.CONTENT_URI, contentValues);
            Log.d("URI_INSERT###", uri.toString());
        }

        int[] views = new int[]{
                R.id.title, R.id.duration,
                R.id.actor
        };

        String[] columns = new String[]{
                MovieContract.Drama.TITLE,  MovieContract.Drama.DURATION,
                MovieContract.Drama.ACTOR
        };

        CursorLoader cursorLoader = new CursorLoader(
                this, MovieContract.SciFi.CONTENT_URI,null,null,null,null
        );

        Cursor c;
        c = cursorLoader.loadInBackground();
        if(c.moveToFirst()){
            do {
                String title = c.getString(c.getColumnIndex(MovieContract.SciFi.TITLE));
                String actor = c.getString(c.getColumnIndex(MovieContract.SciFi.DURATION));
                String duration = c.getString(c.getColumnIndex(MovieContract.SciFi.DURATION));
                String id = c.getString(c.getColumnIndex(MovieContract.SciFi._ID));
                Log.d("URI_CURSOR_VALUES###", title +"###"+ actor +"###"+ duration +"###"+ id);
            }while (c.moveToNext());
        }

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                this, R.layout.row_details, c, columns, views, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        );

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);


        int _id = 1;
        int rowsDeleted;
//        int rowsDeleted = getContentResolver().delete(MovieContract.SciFi.CONTENT_URI, null, null);

        rowsDeleted = getContentResolver().delete(MovieContract.SciFi.CONTENT_URI,
//                MovieContract.Drama.TITLE + " LIKE ?",
                MovieContract.Drama._ID + " >= ?",
//                new String[]{"%a"}); //ending with a
//                new String[]{"a%"}); //beginning with a
//                new String[]{"%a%"}); //contains an 'a' somewhere
                new String[]{"33"}); // without % sign LIKE is the same as WHERE TITLE = android inception 0
        Log.d("CONTENT_DELETE: ", String.valueOf(rowsDeleted));
    }
}
