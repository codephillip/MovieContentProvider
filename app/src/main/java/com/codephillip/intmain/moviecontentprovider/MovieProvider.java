package com.codephillip.intmain.moviecontentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by codephillip on 10/26/15.
 */
public class MovieProvider extends ContentProvider {
    private MovieDbHelper MOVIE_DB_HELPER;


    public static final int DRAMA = 100;
    public static final int DRAMA_WITH_TITLE = 101;
    public static final int SCIFI = 200;
    public static final int SCIFI_WITH_TITLE = 201;

    public static UriMatcher sUriMatcher = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {
        final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = MovieContract.CONTENT_AUTHORITY;

        //connect uris to their integer values
        uriMatcher.addURI(authority, MovieContract.PATH_DRAMA, DRAMA);
        uriMatcher.addURI(authority, MovieContract.PATH_SCIFI+"/*", DRAMA_WITH_TITLE);

        uriMatcher.addURI(authority, MovieContract.PATH_SCIFI, SCIFI);
        uriMatcher.addURI(authority, MovieContract.PATH_SCIFI+"/*", SCIFI_WITH_TITLE);

        return uriMatcher;
    }


    @Override
    public boolean onCreate() {
        MOVIE_DB_HELPER = new MovieDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;
        SQLiteDatabase db = MOVIE_DB_HELPER.getReadableDatabase();
        switch (sUriMatcher.match(uri)){
            case DRAMA:{
                cursor = db.query(
                        MovieContract.Drama.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case DRAMA_WITH_TITLE:{
                cursor = db.query(
                        MovieContract.Drama.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SCIFI:{
                cursor = db.query(
                        MovieContract.SciFi.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                        );
                break;
            }
            case SCIFI_WITH_TITLE:{
                cursor = db.query(
                        MovieContract.SciFi.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        //return a mime after match is found
        switch (sUriMatcher.match(uri)){
            case DRAMA:
                return MovieContract.Drama.CONTENT_TYPE;
            case DRAMA_WITH_TITLE:
                return MovieContract.Drama.CONTENT_ITEM_TYPE;
            case SCIFI:
                return MovieContract.SciFi.CONTENT_TYPE;
            case SCIFI_WITH_TITLE:
                return MovieContract.SciFi.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("UnKnown Uri "+ uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = MOVIE_DB_HELPER.getWritableDatabase();
        Uri returnUri;
        switch (sUriMatcher.match(uri)){
            case DRAMA:{
                long id = db.insert(MovieContract.Drama.TABLE_NAME, null, contentValues);
                //if table insertion was successful the value returned should be greater than 0
                if (id > 0){
                    returnUri =  MovieContract.Drama.buildUri(id);
                }else {
                    throw new android.database.SQLException("Failed to insert into table "+ uri);
                }
                break;
            }
            case SCIFI:{
                long id = db.insert(MovieContract.SciFi.TABLE_NAME, null, contentValues);
                if (id > 0){
                    returnUri = MovieContract.SciFi.buildUri(id);
                }else {
                    throw new android.database.SQLException("Failed to insert into table "+ uri);
                }
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown Uri "+ uri);

        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete (Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = MOVIE_DB_HELPER.getWritableDatabase();
        int rowsDeleted;
        Log.d("CONTENT_DELETE: ","STARTED DELETE");
        //incase selection is null, make selection 1 inorder to delete the whole table
        if (selection == null) selection = "1";
        switch (sUriMatcher.match(uri)){
            case DRAMA:
                rowsDeleted = db.delete(MovieContract.Drama.TABLE_NAME, selection, selectionArgs);
                break;
            case SCIFI:
                rowsDeleted = db.delete(MovieContract.SciFi.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: "+ uri);
        }
        //incase the number of rows deleted is not zero, notify changes to the root uri
        if (rowsDeleted != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
