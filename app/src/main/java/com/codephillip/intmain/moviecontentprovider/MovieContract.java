package com.codephillip.intmain.moviecontentprovider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by codephillip on 10/26/15.
 */
public class MovieContract {
    public static final String CONTENT_AUTHORITY = "com.codephillip.intmain.moviecontentprovider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://"+CONTENT_AUTHORITY );

    public static final String PATH_DRAMA = "drama";
    public static final String PATH_SCIFI = "scifi";

    public static final class Drama implements BaseColumns{
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_DRAMA).build();
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + CONTENT_AUTHORITY;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + CONTENT_AUTHORITY;

        public static final String TABLE_NAME = "drama";
        public static final String TITLE = "title";
        public static final String ACTOR = "actor";
        public static final String DURATION= "duration";

        public static final Uri buildUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI,id);
        }
    }

    public static final class SciFi implements BaseColumns{
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_SCIFI).build();
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + CONTENT_AUTHORITY;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + CONTENT_AUTHORITY;

        public static final String TABLE_NAME = "scifi";
        public static final String TITLE = "title";
        public static final String ACTOR= "actor";
        public static final String DURATION= "duration";

        public static final Uri buildUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
